package online.inote.naruto.common.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * @description Spring工具类
 * @author XQF.Sui
 * @date 2021/07/22 09:43
 */
public class SpringUtils {

  public static Method getMethod(ProceedingJoinPoint point) {
    try {
      Signature signature = point.getSignature();
      MethodSignature ms = (MethodSignature) signature;
      return point.getTarget().getClass().getMethod(ms.getName(), ms.getParameterTypes());
    } catch (NoSuchMethodException e) {
      throw new online.inote.naruto.exception.NoSuchMethodException("获取方法失败", e);
    }
  }

  public static String getMethodName(ProceedingJoinPoint point) {
    return getClass(point).getName() + '.' + getMethod(point).getName();
  }

  public static Class<?> getClass(ProceedingJoinPoint point) {
    return point.getTarget().getClass();
  }

  public static HttpServletRequest getHttpServletRequest() {
    return getServletRequestAttributes().getRequest();
  }

  public static ServletRequestAttributes getServletRequestAttributes() {
    return (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
  }
}
