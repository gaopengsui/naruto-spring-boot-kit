package online.inote.naruto.common.global.enable;

import online.inote.naruto.common.global.GlobalResultHandle;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description 启用全局结果处理
 * @author gaopengsui@creditease.cn
 * @date 2021/11/04 8:11 下午
 */
@Import(GlobalResultHandle.class)
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableGlobalResultHandle {}
