package online.inote.naruto.common.utils.response;

import online.inote.naruto.utils.StringUtils;

/**
 * @description 扩展响应信息
 * @author gaopengsui@creditease.cn
 * @date 2021/09/08 16:56
 */
public class ExtendResponse<T> extends I18nResponse<T> {

  private static final long serialVersionUID = 4713365336941955492L;

  ExtendResponse() {
    super();
  }

  public ExtendResponse(String code, String message) {
    super(code, message);
  }

  public ExtendResponse(String code, String message, T data) {
    super(code, message, data);
  }

  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 以下方法支持i18n -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  public static <T> ExtendResponse<T> success() {
    return new ExtendResponse<>(Code.SUCCESS.getCode(), Code.SUCCESS.getMessage());
  }

  public static <T> ExtendResponse<T> success(T data) {
    return new ExtendResponse<>(Code.SUCCESS.getCode(), Code.SUCCESS.getMessage(), data);
  }

  public static <T> ExtendResponse<T> fail() {
    return new ExtendResponse<>(Code.FAIL.getCode(), Code.FAIL.getMessage());
  }

  // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- 以下方法不支持i18n -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

  public static <T> ExtendResponse<T> success(final T data, final String message) {
    return new ExtendResponse<>(Code.SUCCESS.getCode(), message, data);
  }

  public static <T> ExtendResponse<T> fail(final String message) {
    return new ExtendResponse<>(Code.FAIL.getCode(), message);
  }

  public static <T> ExtendResponse<T> fail(String messageTemplate, final Object... args) {
    return new ExtendResponse<>(Code.FAIL.getCode(), StringUtils.format(messageTemplate, args));
  }

  public static <T> ExtendResponse<T> create(final Code code, String message, final T data) {
    return new ExtendResponse<>(code.getCode(), message, data);
  }
}
