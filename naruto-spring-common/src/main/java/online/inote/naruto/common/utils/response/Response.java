package online.inote.naruto.common.utils.response;

import lombok.experimental.FieldNameConstants;

import java.io.Serializable;

/**
 * @description 响应信息
 * @author gaopengsui@creditease.cn
 * @date 2021/09/08 11:24
 */
@FieldNameConstants
public class Response<T> implements Serializable {

  private static final long serialVersionUID = 1940009865956053614L;

  public String code;
  public String message;
  public T data;

  public Response() {}

  public Response(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public Response(String code, String message, T data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  public static <T> Response<T> create(String code, String message) {
    return new Response<>(code, message);
  }

  public static <T> Response<T> create(String code, String message, T data) {
    return new Response<>(code, message, data);
  }
}
