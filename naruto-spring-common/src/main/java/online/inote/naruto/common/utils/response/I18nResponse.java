package online.inote.naruto.common.utils.response;

import online.inote.naruto.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;

/**
 * @description 响应信息(国际化)
 * @author gaopengsui@creditease.cn
 * @date 2021/09/08 11:21
 */
public class I18nResponse<T> extends Response<T> {

  private static final long serialVersionUID = -3297318100457726019L;

  I18nResponse() {
    super();
  }

  I18nResponse(String code, String message) {
    super(code, message);
  }

  I18nResponse(String code, String message, T data) {
    super(code, message, data);
  }

  public static <T> I18nResponse<T> create(Code code) {
    return new I18nResponse<>(code.getCode(), code.getMessage());
  }

  public static <T> I18nResponse<T> create(String code, Object... args) {
    return new I18nResponse<>(code, Code.getMessage(code, args));
  }

  public static <T> I18nResponse<T> create(Code code, Object... args) {
    return new I18nResponse<>(code.getCode(), code.getMessage(args));
  }

  public boolean isSuccess() {
    return CollectionUtils.containsAny(ResponseI18nProperties.props().getSuccessCode(), this.code);
  }

  public String getMessage() {
    if (StringUtils.isNotBlank(this.message)) {
      return this.message;
    }

    if (StringUtils.isNotBlank(this.code)) {
      return Code.getMessage(this.code);
    }

    return null;
  }

  public String getI18nMessage() {
    if (StringUtils.isNotBlank(this.code)) {
      return Code.getMessage(this.code);
    }

    return null;
  }
}
