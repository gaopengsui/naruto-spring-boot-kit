package online.inote.naruto.common.utils.bean;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description 启用BeanFactory
 * @author gaopengsui@creditease.cn
 * @date 2021/09/09 18:00
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(BeanFactory.class)
public @interface EnableBeanFactory {}
