package online.inote.naruto.common.utils.i18n.spring.boot.annotation;

import online.inote.naruto.common.utils.i18n.spring.boot.config.I18nConfigurer;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description 启用i18n
 * @author gaopengsui@creditease.cn
 * @date 2021/09/09 16:31
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(I18nConfigurer.class)
public @interface EnableI18n {}
