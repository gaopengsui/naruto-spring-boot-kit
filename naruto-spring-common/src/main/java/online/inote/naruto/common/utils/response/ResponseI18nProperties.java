package online.inote.naruto.common.utils.response;

import lombok.Data;
import online.inote.naruto.common.utils.bean.BeanFactory;
import online.inote.naruto.common.utils.bean.EnableBeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @description i18n配置
 * @author gaopengsui@creditease.cn
 * @date 2021/09/09 17:25
 */
@Data
@EnableBeanFactory
@ConfigurationProperties(prefix = ResponseI18nProperties.I18N_PREFIX)
public class ResponseI18nProperties {

  protected static final String I18N_PREFIX = "system.response.i18n";

  private static ResponseI18nProperties props;

  private String responseBundle = "i18n/response/common_response";
  /** 成功CODE,默认为 A10001 */
  private List<String> successCode =
      new ArrayList<String>() {
        private static final long serialVersionUID = -2760845726795830823L;

        {
          add("A10001");
        }
      };

  public static ResponseI18nProperties props() {
    if (Objects.isNull(props)) {
      synchronized (ResponseI18nProperties.class) {
        if (Objects.isNull(props)) {
          props = BeanFactory.getBean(ResponseI18nProperties.class);
        }
      }
    }

    return props;
  }
}
