package online.inote.naruto.common.utils.response;

import online.inote.naruto.common.utils.i18n.I18n;
import online.inote.naruto.utils.StringUtils;

/**
 * @description 响应信息CODE
 * @author gaopengsui@creditease.cn
 * @date 2021/09/08 15:18
 */
public class Code {

  /** A10001 成功 */
  public static final Code SUCCESS = Code.of("A10001");

  /** A10002 失败 */
  public static final Code FAIL = Code.of("A10002");

  /** A10003 参数必填 */
  public static final Code PARAM_REQUIRED = Code.of("A10003");

  /** A10004 该信息不存在 */
  public static final Code NOT_EXISTS = Code.of("A10004");

  /** A10005 该信息已存在 */
  public static final Code EXISTS = Code.of("A10005");

  /** A10006 有权限 */
  public static final Code OWN_PERMISSION = Code.of("A10006");

  /** A10007 没有权限 */
  public static final Code NOT_PERMISSION = Code.of("A10007");

  /** A50008 非法令牌 */
  public static final Code TOKEN_ILLEGAL = Code.of("A50008");

  /** A50014 登录超时 */
  public static final Code TOKEN_EXPIRED = Code.of("A50014");

  private final String code;

  public String getCode() {
    return this.code;
  }

  protected Code(String code) {
    this.code = code;
  }

  public String getMessage(Object... args) {
    return StringUtils.format(I18n.message(getResponseBundle(), code), args);
  }

  public static String getMessage(String code, Object... args) {
    return StringUtils.format(I18n.message(getResponseBundle(), code), args);
  }

  public static String getResponseBundle() {
    return ResponseI18nProperties.props().getResponseBundle();
  }

  public static Code of(String code) {
    return new Code(code);
  }
}
