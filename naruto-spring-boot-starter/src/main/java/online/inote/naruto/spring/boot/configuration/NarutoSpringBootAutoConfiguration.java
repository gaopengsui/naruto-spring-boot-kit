package online.inote.naruto.spring.boot.configuration;

import online.inote.naruto.common.utils.bean.EnableBeanFactory;
import online.inote.naruto.entrance.core.EnableNarutoEntrance;
import org.springframework.context.annotation.Configuration;

/**
 * @description 自动配置
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 4:08 下午
 */
@Configuration
@EnableBeanFactory
@EnableNarutoEntrance
public class NarutoSpringBootAutoConfiguration {}
