package online.inote.naruto.annotation.token;

import java.lang.annotation.*;

/**
 * @description Token安全校验注解
 * @author gaopengsui@creditease.cn
 * @date 2020/8/25 11:21 上午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface WebSecurity {

  /** 是否校验 */
  boolean required() default true;
}
