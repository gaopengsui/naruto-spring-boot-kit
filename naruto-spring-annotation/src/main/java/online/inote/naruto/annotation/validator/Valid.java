package online.inote.naruto.annotation.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @description 参数验证
 * @author gaopengsui@creditease.cn
 * @date 2019年6月22日 下午7:34:21
 */
@Documented
@Retention(RUNTIME)
@Target(METHOD)
public @interface Valid {

  Class<?> validator();

  String method() default "";

  Class<?>[] paramType() default {};

  Class<?> modelType() default String.class;

  boolean required() default true;
}
