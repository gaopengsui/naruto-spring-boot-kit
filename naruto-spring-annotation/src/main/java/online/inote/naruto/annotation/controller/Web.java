package online.inote.naruto.annotation.controller;

import java.lang.annotation.*;

/**
 * @description 项目入口Web注解
 * @author gaopengsui@creditease.cn
 * @date 2021/10/19 5:59 下午
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Web {}
