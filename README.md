# naruto-spring-boot-kit

#### 介绍

SpringBoot项目开发工具包。

#### 使用说明

1. 引入jar包

   ```xml
   <dependency>
     <groupId>online.inote</groupId>
     <artifactId>naruto-spring-boot-starter</artifactId>
     <version>0.9.0-RELEASE</version>
   </dependency>
   ```


2. **配置项目统一入口**(jar包引入默认开启)，需要在配置中增加以下配置：

   ```properties
   # 默认配置 /api/** 为项目间调用入口
   system.entrance.api.context-path=api/
   # 默认配置 /web/** 为运营后台请求入口
   system.entrance.web.context-path=web/
   ```

   Controller类需要增加`@Web`或`@Api`注解，例如：

   ```java
   @Slf4j
   @Web
   @RestController
   @RequestMapping(value = "demo")
   public class DemoController {
   
     @GetMapping(value = "test")
     public String test() {
       log.info("Demo test.");
       return "Demo test.";
     }
   }
   ```

   至此访问路径由`http://{{ip}}:{{port}}/naruto-sample/xxx/xxx`变为`http://{{ip}}:{{port}}/naruto-sample/web/xxx/xxx`

3. **启用Web安全认证**（默认不开启），

    1. 需在项目启动类增加注解`@EnableWebSecurity`

       ```java
       @EnableWebSecurity
       @SpringBootApplication
       public class NarutoApplication {
       
         public static void main(String[] args) {
           SpringApplication.run(NarutoApplication.class, args);
         }
       }
       ```


2. 增加配置：

   ```properties
   # 认证拦截的路径
   system.web.security.token.intercept-context-path=/web/**
   # token生成的秘钥
   system.web.security.token.secret-key=eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ
   ```


4. 启用统一结果处理，项目启动类增加注解`@EnableGlobalResultHandle`，返回具体内容见工具类Response相关内容。

   ```java
   @SpringBootApplication
   @EnableGlobalResultHandle
   public class NarutoApplication {
   
     public static void main(String[] args) {
       SpringApplication.run(NarutoApplication.class, args);
     }
   }
   ```


5. 基础工具类

    1. `BeanFactory`：默认开启，可以直接通过BeanFactory的静态方法直接获取Spring管理的Bean。

       `Object getBean(String name)`：根据bean name获取Bean。

       `T getBean(Class<T> clazz)`：根据Class获取指定Bean。

       `T getBean(String name, Class<T> clazz)`:根据bean name及Class获取指定Bean。

       `Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> clazz)`：获取被指定注解注解的Bean。

       `Map<String, T> getBeansWithType(Class<T> clazz)`：根据指定类型获取Bean集合。


2. `I18n`：国际化，默认关闭，如果有国际化需求，需在项目启动类增加注解`@EnableI18n`。


3. `Response`：标准返回结果对象

   | 字段    | 类型   | 内容         |
            | ------- | ------ | ------------ |
   | code    | String | 结果标识     |
   | message | String | 返回提示信息 |
   | data    | T      | 结果数据     |


4. `I18nResponse`：国际化结果对象，继承自`Response`，支持`message`国际化。

5. `ExtendResponse`：扩展结果对象，继承自`I18nResponse`，新增成功、失败方法。

6. `SpringBootBindUtils`：SpringBoot配置绑定工具。


6. 版本说明

   `0.9.0-RELEASE`：

    - 支持统一入口配置

    - 支持统一结果处理、全局异常处理

    - 支持基础Token的安全认证

    - 支持国际化

    - 封装部分Springboot相关工具类


7. 详细使用方法参考模块`naruto-spring-boot-starter`。


