package online.inote.naruto.entrance.core;

import lombok.extern.slf4j.Slf4j;
import online.inote.naruto.annotation.controller.Api;
import online.inote.naruto.annotation.controller.Web;
import online.inote.naruto.entrance.props.EntranceProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description 项目入口配置
 * @author gaopengsui@creditease.cn
 * @date 2021/10/19 7:27 下午
 */
@Slf4j
@Configuration
public class ProjectEntranceConfigurer implements WebMvcConfigurer {

  private final EntranceProperties props;

  public ProjectEntranceConfigurer(EntranceProperties props) {
    this.props = props;
  }

  @Override
  public void configurePathMatch(PathMatchConfigurer configurer) {
    log.info(
        "配置系统入口:[ {} ], [ {} ]", props.getApi().getContextPath(), props.getWeb().getContextPath());
    configurer
        .addPathPrefix(props.getApi().getContextPath(), c -> c.isAnnotationPresent(Api.class))
        .addPathPrefix(props.getWeb().getContextPath(), c -> c.isAnnotationPresent(Web.class));
  }
}
