package online.inote.naruto.entrance.props;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @description 系统配置
 * @author gaopengsui@creditease.cn
 * @date 2021/02/26 18:10
 */
@Data
@Component
@ConfigurationProperties(prefix = "system.entrance")
public class EntranceProperties {

  private Api api;
  private Web web;

  @Setter
  @Getter
  public static class Api {
    private String contextPath;
  }

  @Setter
  @Getter
  public static class Web {
    private String contextPath;
  }
}
