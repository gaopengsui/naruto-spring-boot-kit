package online.inote.naruto.entrance.core;

import online.inote.naruto.entrance.props.EntranceProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description 启用项目入口配置
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 4:33 下午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({EntranceProperties.class, ProjectEntranceConfigurer.class})
public @interface EnableNarutoEntrance {}
