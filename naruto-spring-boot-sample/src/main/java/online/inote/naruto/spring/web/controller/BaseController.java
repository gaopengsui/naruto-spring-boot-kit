package online.inote.naruto.spring.web.controller;

import online.inote.naruto.security.props.TokenProperties;
import online.inote.naruto.security.utils.JwtHelper;
import online.inote.naruto.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * @description 基础Controller
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 6:44 下午
 */
public class BaseController extends WebController {

  @Autowired protected HttpServletRequest request;
  @Autowired protected TokenProperties props;

  protected String getToken() {
    return request.getHeader(props.getHeader().getKey());
  }

  protected String getUserId() {
    String token = getToken();

    if (StringUtils.isBlank(token)) {
      return null;
    }

    return JwtHelper.getClaims(token).getBody().getId();
  }
}
