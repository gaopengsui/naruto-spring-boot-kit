package online.inote.naruto.spring.web.controller;

import online.inote.naruto.annotation.controller.Web;

/**
 * @description 后台Controller
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 6:47 下午
 */
@Web
public class WebController {}
