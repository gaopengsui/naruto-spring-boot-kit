package online.inote.naruto.spring.web.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @description 用户实体对象
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 6:33 下午
 */
@Data
public class UserEntity implements Serializable {

  private static final long serialVersionUID = -1631057666068502033L;

  private String id;
  private String username;
  private String password;
  private String privateKey;
}
