package online.inote.naruto.spring.web.controller;

import lombok.extern.slf4j.Slf4j;
import online.inote.naruto.annotation.controller.Web;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description 实例Controller
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 6:20 下午
 */
@Slf4j
@Web
@RestController
@RequestMapping(value = "demo")
public class DemoController {

  @GetMapping(value = "test")
  public String test() {
    log.info("Demo test.");
    return "Demo test.";
  }
}
