package online.inote.naruto.spring;

import online.inote.naruto.common.global.enable.EnableGlobalResultHandle;
import online.inote.naruto.security.core.EnableWebSecurity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 启动类
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 5:52 下午
 */
@EnableWebSecurity
@SpringBootApplication
@EnableGlobalResultHandle
public class NarutoApplication {

  public static void main(String[] args) {
    SpringApplication.run(NarutoApplication.class, args);
  }
}
