package online.inote.naruto.cache.redis;

import online.inote.naruto.annotation.Priority;
import online.inote.naruto.cache.CacheManager;
import online.inote.naruto.common.utils.bean.BeanFactory;
import online.inote.naruto.utils.Assert;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @description Redis缓存管理器
 * @author XQF.Sui
 * @date 2021/07/30 00:02
 */
@Priority
public class NarutoRedisCacheManager implements CacheManager {

  private static final String DEFAULT_VALUE = "1";

  private StringRedisTemplate redisTemplate = BeanFactory.getBean(StringRedisTemplate.class);

  @Override
  public void cache(String key, String value) {
    Assert.notBlank(key, "key不能为空");
    Assert.notBlank(value, "value不能为空");
    redisTemplate.opsForValue().set(key, value);
  }

  @Override
  public void cache(String key, long ttl) {
    Assert.notBlank(key, "key不能为空");
    Assert.isTrue(ttl > 0, "ttl不能小于1");
    redisTemplate.opsForValue().set(key, DEFAULT_VALUE, ttl, TimeUnit.SECONDS);
  }

  @Override
  public void cache(String key, String value, long ttl) {
    Assert.notBlank(key, "key不能为空");
    Assert.notBlank(value, "value不能为空");
    Assert.isTrue(ttl > 0, "ttl不能小于1");
    redisTemplate.opsForValue().set(key, value, ttl, TimeUnit.SECONDS);
  }

  @Override
  public Long increment(String key) {
    Assert.notBlank(key, "key不能为空");
    return redisTemplate
        .opsForValue()
        .increment(key, online.inote.naruto.cache.CacheManager.VERSION_INCREMENT_STEP);
  }

  @Override
  public boolean isExist(String key) {
    Assert.notBlank(key, "key不能为空");
    return redisTemplate.hasKey(key);
  }

  @Override
  public void delete(String key) {
    Assert.notBlank(key, "key不能为空");
    redisTemplate.delete(key);
  }

  @Override
  public String get(String key) {
    Assert.notBlank(key, "key不能为空");
    return redisTemplate.opsForValue().get(key);
  }

  @Override
  public Boolean expire(String key, long ttl) {
    return expire(key, ttl, TimeUnit.SECONDS);
  }

  @Override
  public Boolean expire(String key, long ttl, TimeUnit timeUnit) {
    return redisTemplate.expire(key, ttl, timeUnit);
  }

  @Override
  public Long addSet(String key, String... values) {
    Assert.notBlank(key, "key不能为空");
    Assert.notNull(values, "values不能为空");
    return redisTemplate.opsForSet().add(key, values);
  }

  @Override
  public Long addSet(String key, Set<String> set) {
    Assert.notBlank(key, "key不能为空");
    Assert.notEmpty(set, "set不能为空");
    return redisTemplate.opsForSet().add(key, set.toArray(new String[set.size()]));
  }

  @Override
  public Long remove(String key, String... values) {
    Assert.notBlank(key, "key不能为空");
    Assert.notNull(values, "values不能为空");
    return redisTemplate.opsForSet().remove(key, values);
  }

  @Override
  public Boolean move(String key, String value, String destKey) {
    Assert.notBlank(key, "key不能为空");
    Assert.notBlank(value, "value不能为空");
    Assert.notBlank(destKey, "destKey不能为空");
    return redisTemplate.opsForSet().move(key, value, destKey);
  }

  @Override
  public Set<String> members(String key) {
    Assert.notBlank(key, "key不能为空");
    return redisTemplate.opsForSet().members(key);
  }

  @Override
  public Boolean isMember(String key, String value) {
    Assert.notBlank(key, "key不能为空");
    Assert.notBlank(value, "value不能为空");
    return redisTemplate.opsForSet().isMember(key, value);
  }

  @Override
  public Long count(String key) {
    Assert.notBlank(key, "key不能为空");
    return redisTemplate.opsForSet().size(key);
  }
}
