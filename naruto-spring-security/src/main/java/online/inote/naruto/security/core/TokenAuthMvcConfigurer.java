package online.inote.naruto.security.core;

import online.inote.naruto.security.props.TokenProperties;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description
 * @author gaopengsui@creditease.cn
 * @date 2019年4月13日 上午9:05:49
 */
@Configuration
public class TokenAuthMvcConfigurer implements WebMvcConfigurer {

  private static final String DEFAULT_CONTEXT_PATH = "/web/**";
  private final TokenProperties props;

  public TokenAuthMvcConfigurer(TokenProperties props) {
    this.props = props;
  }

  @Bean
  TokenHandlerInterceptor tokenHandlerInterceptor() {
    return new TokenHandlerInterceptor();
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    if (CollectionUtils.isNotEmpty(props.getInterceptContextPath())) {
      registry
          .addInterceptor(tokenHandlerInterceptor())
          .addPathPatterns(props.getInterceptContextPath());
    } else {
      registry.addInterceptor(tokenHandlerInterceptor()).addPathPatterns(DEFAULT_CONTEXT_PATH);
    }
  }
}
