package online.inote.naruto.security.props;

import lombok.Data;
import online.inote.naruto.common.utils.bean.BeanFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @description Token配置
 * @author gaopengsui@creditease.cn
 * @date 2021/09/30 5:22 下午
 */
@Data
@Component
@ConfigurationProperties(prefix = TokenProperties.TOKEN_PROPERTIES_PREFIX)
public class TokenProperties {

  public static final String TOKEN_PROPERTIES_PREFIX = "system.web.security.token";

  private Header header = new Header();
  private Cache cache = new Cache();
  private String secretKey;
  private List<String> interceptContextPath = new ArrayList<String>();

  @Data
  public class Header {
    private String key = "token";
  }

  @Data
  public class Cache {
    private String prefix = "WEB:EMPLOYEE:LOGIN_TOKEN_";
    private long expireTime = 1800;
  }

  public static TokenProperties props() {
    return BeanFactory.getBean(TokenProperties.class);
  }
}
