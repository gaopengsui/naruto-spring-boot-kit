package online.inote.naruto.security.core;

import online.inote.naruto.entrance.props.EntranceProperties;
import online.inote.naruto.security.props.TokenProperties;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @description 启用安全配置
 * @author gaopengsui@creditease.cn
 * @date 2021/10/20 4:28 下午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import({EntranceProperties.class, TokenProperties.class, TokenAuthMvcConfigurer.class})
public @interface EnableWebSecurity {}
